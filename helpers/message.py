from flask import jsonify
import requests
from helpers.database import *
import re
import json

def sendAttachment(filePath,caption):
  return jsonify({"type": "attachment","data": filePath, "caption" : caption}), 200


def sendMessage(isiMessage):
  return jsonify({"type": "message","data": isiMessage}), 200

def waKirimPesan(nomor_telepon, message):
    url = f"http://{config.wa_api_be}:5002/kirimpesan"
    data = {"nomor_telepon": nomor_telepon, "pesan":message}
    try:
      x = requests.post(url,timeout = 5, json=data)
      return True
    except Exception as e:
      return False
    

def waKirimSurat(nomor_telepon, pesan, filedata, mime_type):
    url = f"http://{config.wa_api_be}:5002/kirimfile"
    data = {"nomor_telepon": nomor_telepon, "pesan":pesan, "mime_type":mime_type, "filedata":filedata}
    x = requests.post(url,json=data)
    return x.text

def waCekStatus():
    url = f"http://{config.wa_api_be}:5002/"
    try:
      req = requests.get(url, timeout = 2).json()
      return req
    except Exception as e:
      print(e)
      return [{"nomor":"Status","status":"Tolong Cek Handphone"}]

def waGetSession(hp):
    url = f"http://{config.wa_api_be}:5002/getsession"
    data = {"hp": hp}
    try:
      x = requests.post(url,timeout = 5, json=data)
      return x.text
    except Exception as e:
      return False

def dataPercent(part, whole):
  try:
    x = 100 * float(part)/float(whole)
    return "{:.1f}".format(x)
  except:
    return 0
    
def cek_nomor_telepon(value):
    rule = re.compile(r'^(?:\+?62)?[06]\d{9,13}$')
    if not rule.search(value):
        print(f"nomor salah: {value}")
        return False
    else:
        print(f"nomor benar: {value}")
        return True