import pymysql
import datetime
import config.config as config


def getLoginData(email):
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        dbCursor.execute(
            f"SELECT nama, email, password from users WHERE email='{email}'"
        )
        hasil = dbCursor.fetchone()
        if not hasil:
            dbCursor.close()
            return False
        else:
            return hasil


def logs(message_name, message_from, keterangan):
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        sql = "INSERT INTO logs (id, nama, nomor_telepon, keterangan, created_at) VALUES (0, %s, %s,%s, NOW())"
        val = (message_name, message_from, keterangan)
        dbCursor.execute(sql, val)
        try:
            dbbaru.commit()
            dbCursor.close()
            return True
        except:
            dbCursor.close()
            return False


def insertDataPMB(nama, asal_sekolah, nomor_telepon, email, jenis, jalur):
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        sql = "INSERT INTO pmb (id, nama, asal_sekolah, nomor_telepon, email, jenis, jalur) VALUES (0, %s ,%s, %s, %s, %s, %s)"
        val = (nama, asal_sekolah, nomor_telepon, email, jenis, jalur)
        dbCursor.execute(sql, val)
        try:
            dbbaru.commit()
            dbCursor.close()
            return True
        except:
            return False


def insertDataPMBJPA(
    nama_lengkap,
    nisn,
    password,
    jenis_kelamin,
    no_telepon,
    email,
    perguruan_tinggi,
    jalur,
    no_transaksi,
    virtual_account,
    jumlah_tagihan,
    tgl_expired_va,
    status_pembayaran,
    tanggal_daftar,
):
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        sql = "INSERT INTO pmb_jpa (id, nama_lengkap, nisn, password, jenis_kelamin, no_telepon, email, perguruan_tinggi, jalur, no_transaksi, virtual_account, jumlah_tagihan, tgl_expired_va, status_pembayaran, tanggal_daftar) VALUES (0, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        val = (
            nama_lengkap,
            nisn,
            password,
            jenis_kelamin,
            no_telepon,
            email,
            perguruan_tinggi,
            jalur,
            no_transaksi,
            virtual_account,
            jumlah_tagihan,
            tgl_expired_va,
            status_pembayaran,
            tanggal_daftar,
        )
        dbCursor.execute(sql, val)
        try:
            dbbaru.commit()
            dbCursor.close()
            return True
        except Exception as e:
            print(e)
            return False


def insertDataPMBSertifikat(nama, asal_sekolah, nomor_telepon, email, jenis, jalur):
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        sql = "INSERT INTO pmb_sertifikat (id, nama, asal_sekolah, nomor_telepon, email, jenis, jalur) VALUES (0, %s ,%s, %s, %s, %s, %s)"
        val = (nama, asal_sekolah, nomor_telepon, email, jenis, jalur)
        dbCursor.execute(sql, val)
        try:
            dbbaru.commit()
            dbCursor.close()
            return True
        except:
            return False


def insertDataEventBLAST(nama_event, path_file, attachment, isi_email):
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        if path_file == None:
            sql = f"INSERT INTO events (nama_event, path_file, attachment, isi_email) VALUES ('{nama_event}',NULL , NULL, '{isi_email}');"
        else:
            sql = f"INSERT INTO events (nama_event, path_file, attachment, isi_email) VALUES ('{nama_event}', '{path_file}' ,'{attachment}', '{isi_email}');"
        dbCursor.execute(sql)
        try:
            dbbaru.commit()
            dbCursor.close()
            return True
        except:
            return False


def deleteDataEvent(id):
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        sql = f"DELETE FROM events WHERE id={id}"
        dbCursor.execute(sql)
        try:
            dbbaru.commit()
            dbCursor.close()
            return True
        except:
            return False


def lihatDataPMB():
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        dbCursor.execute(f"SELECT * FROM pmb")
        hasil = dbCursor.fetchall()
        if not hasil:
            dbCursor.close()
            return False
        else:
            dbCursor.close()
            return hasil


def lihatDataPMBJPA():
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        dbCursor.execute(f"SELECT * FROM pmb_jpa WHERE jalur='JPA'")
        hasil = dbCursor.fetchall()
        if not hasil:
            dbCursor.close()
            return False
        else:
            dbCursor.close()
            return hasil


def lihatDataPMBUndanganSertifikat():
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        dbCursor.execute(f"SELECT * FROM pmb_sertifikat")
        hasil = dbCursor.fetchall()
        if not hasil:
            dbCursor.close()
            return False
        else:
            dbCursor.close()
            return hasil


def lihatDataEvent():
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        dbCursor.execute(f"SELECT * FROM events")
        hasil = dbCursor.fetchall()
        if not hasil:
            dbCursor.close()
            return False
        else:
            dbCursor.close()
            return hasil


def lihatDataPMBUndanganSertifikat():
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        dbCursor.execute(f"SELECT * FROM pmb_sertifikat")
        hasil = dbCursor.fetchall()
        if not hasil:
            dbCursor.close()
            return False
        else:
            dbCursor.close()
            return hasil


def lihatLogs():
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        dbCursor.execute(f"SELECT * FROM logs")
        hasil = dbCursor.fetchall()
        if not hasil:
            dbCursor.close()
            return False
        else:
            dbCursor.close()
            return hasil


def checkLogsPengiriman(nomor_telepon):
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        dbCursor.execute(
            f"SELECT * FROM logs_pesan WHERE nomor_telepon = '{nomor_telepon}'"
        )
        hasil = dbCursor.fetchone()
        if not hasil:
            dbCursor.close()
            return False
        else:
            dbCursor.close()
            return True


def cekDataSudahAda(nama):
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        dbCursor.execute(f'SELECT * FROM pmb WHERE nama = "{str(nama)}"')
        hasil = dbCursor.fetchall()
        if not hasil:
            dbCursor.close()
            return False
        else:
            dbCursor.close()
            return True


def cekDataSudahAdaJPA(nama):
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        dbCursor.execute(f'SELECT * FROM pmb_jpa WHERE nama_lengkap = "{str(nama)}"')
        hasil = dbCursor.fetchall()
        if not hasil:
            dbCursor.close()
            return False
        else:
            dbCursor.close()
            return True


def cariDataPMB(id):
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        dbCursor.execute(f"SELECT * FROM pmb WHERE id = {id}")
        hasil = dbCursor.fetchall()
        if not hasil:
            dbCursor.close()
            return False
        else:
            dbCursor.close()
            return hasil


def cariDataPMBTahun(tahun, jenis, jalur):
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        dbCursor.execute(
            f"SELECT * FROM pmb WHERE tahun = {tahun} AND status = 'Belum Dikirim' AND jenis = '{jenis}' AND jalur = '{jalur}'"
        )
        hasil = dbCursor.fetchall()
        if not hasil:
            dbCursor.close()
            return False
        else:
            dbCursor.close()
            return hasil


def cariDataPMBFollowup(tahun, jenis, jalur):
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        dbCursor.execute(
            f"SELECT * FROM pmb WHERE tahun = {tahun} AND jenis = '{jenis}' AND jalur = '{jalur}'"
        )
        hasil = dbCursor.fetchall()
        if not hasil:
            dbCursor.close()
            return False
        else:
            dbCursor.close()
            return hasil


def cariDataEvent(id):
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        dbCursor.execute(f"SELECT * FROM events where id = {id}")
        hasil = dbCursor.fetchone()
        if not hasil:
            dbCursor.close()
            return False
        else:
            dbCursor.close()
            return hasil


def getTemplatePesan(jalur):
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        dbCursor.execute(
            f"SELECT value FROM format WHERE `key` = '{jalur}' ORDER BY RAND() LIMIT 1"
        )
        hasil = dbCursor.fetchone()
        if not hasil:
            dbCursor.close()
            return False
        else:
            dbCursor.close()
            return hasil["value"]


def getDataTemplatePesanAll():
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        dbCursor.execute(f"SELECT * FROM format")
        hasil = dbCursor.fetchall()
        if not hasil:
            dbCursor.close()
            return False
        else:
            dbCursor.close()
            return hasil


def getDataKeywordAll():
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        dbCursor.execute(f"SELECT * FROM keywords")
        hasil = dbCursor.fetchall()
        if not hasil:
            dbCursor.close()
            return False
        else:
            dbCursor.close()
            return hasil


def cariDataTemplate(id):
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        dbCursor.execute(f"SELECT * FROM format WHERE id = {id}")
        hasil = dbCursor.fetchone()
        if not hasil:
            dbCursor.close()
            return False
        else:
            dbCursor.close()
            return hasil


def insertKeywordChatbot(keyword, value):
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        sql = f"INSERT INTO keywords VALUES (0, '{keyword.lower()}', '{value}')"
        dbCursor.execute(sql)
        try:
            dbbaru.commit()
            dbCursor.close()
            return True
        except:
            return False


def insertTemplateBlast(type, value):
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        sql = f"INSERT INTO format VALUES (0, '{type}', '{value}')"
        dbCursor.execute(sql)
        try:
            dbbaru.commit()
            dbCursor.close()
            return True
        except:
            return False


def cariDataKeyword(id):
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        dbCursor.execute(f"SELECT * FROM keywords WHERE id = {id}")
        hasil = dbCursor.fetchone()
        if not hasil:
            dbCursor.close()
            return False
        else:
            dbCursor.close()
            return hasil


def updateTemplateBlast(key, value, id):
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        sql = f"UPDATE markicot.format SET `key`='{key}', `value`='{value}' WHERE  `id`={int(id)}"
        dbCursor.execute(sql)
        try:
            dbbaru.commit()
            dbCursor.close()
            return True
        except:
            return False


def updateKeywordChatbot(keyword, respond, id):
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        sql = f"UPDATE `markicot`.`keywords` SET `keyword`='{keyword}', `respond`='{respond}' WHERE  `id`={int(id)}"
        dbCursor.execute(sql)
        try:
            dbbaru.commit()
            dbCursor.close()
            return True
        except:
            return False


def updatePengirimanSertifikat(id):
    dbbaru = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    with dbbaru:
        dbCursor = dbbaru.cursor(pymysql.cursors.DictCursor)
        sql = f"UPDATE pmb_sertifikat SET pengiriman = 1 WHERE `id`={int(id)}"
        dbCursor.execute(sql)
        try:
            dbbaru.commit()
            dbCursor.close()
            return True
        except:
            return False
