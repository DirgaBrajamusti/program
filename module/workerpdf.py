from module.pmb import *
from PyPDF2 import PdfFileWriter, PdfFileReader
import io
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont

    
def generateSuratUndangan(nama, asal_sekolah):
    packet = io.BytesIO()
    can = canvas.Canvas(packet, pagesize=A4)
    can.setFont('Times-BoldItalic', 9)
    can.drawString(72, 647, nama)
    can.drawString(72, 636, asal_sekolah)
    can.save()
    packet.seek(0)
    new_pdf = PdfFileReader(packet)
    existing_pdf = PdfFileReader(open("assets/template/Template Surat Undangan.pdf", "rb"))
    output = PdfFileWriter()
    page = existing_pdf.getPage(0)
    page.mergePage(new_pdf.getPage(0))
    output.addPage(page)
    outputStream = open(f"assets/template/Surat Undangan {nama}.pdf", "wb")
    output.write(outputStream)
    outputStream.close()
    return True

def generateSuratUndanganPoltekpos(nama, asal_sekolah):
    packet = io.BytesIO()
    can = canvas.Canvas(packet, pagesize=A4)
    can.setFont('Times-BoldItalic', 9)
    can.drawString(72, 647, nama)
    can.drawString(72, 636, asal_sekolah)
    can.save()
    packet.seek(0)
    new_pdf = PdfFileReader(packet)
    existing_pdf = PdfFileReader(open("assets/template/Template Surat Undangan Poltekpos.pdf", "rb"))
    output = PdfFileWriter()
    page = existing_pdf.getPage(0)
    page.mergePage(new_pdf.getPage(0))
    output.addPage(page)
    outputStream = open(f"assets/temp/Surat Kelulusan {nama}.pdf", "wb")
    output.write(outputStream)
    outputStream.close()
    return True

def generateSuratUndanganStimlog(nama, asal_sekolah):
    packet = io.BytesIO()
    can = canvas.Canvas(packet, pagesize=A4)
    can.setFont('Times-BoldItalic', 9)
    can.drawString(72, 647, nama)
    can.drawString(72, 636, asal_sekolah)
    can.save()
    packet.seek(0)
    new_pdf = PdfFileReader(packet)
    existing_pdf = PdfFileReader(open("assets/template/Template Surat Undangan Stimlog.pdf", "rb"))
    output = PdfFileWriter()
    page = existing_pdf.getPage(0)
    page.mergePage(new_pdf.getPage(0))
    output.addPage(page)
    outputStream = open(f"assets/temp/Surat Kelulusan {nama}.pdf", "wb")
    output.write(outputStream)
    outputStream.close()
    return True

def generateSuratJPAPoltepos(nama, nisn, password, nomor_va):
    packet = io.BytesIO()
    can = canvas.Canvas(packet, pagesize=A4)
    can.setFont('Times-Bold', 10)
    can.drawString(207, 551, nama)
    can.drawString(207, 525, nisn)
    can.drawString(207, 498, password)
    can.drawString(207, 471, nomor_va)
    can.save()
    packet.seek(0)
    new_pdf = PdfFileReader(packet)
    existing_pdf = PdfFileReader(open("assets/template/Template Surat Undangan JPA Poltekpos.pdf", "rb"))
    output = PdfFileWriter()
    page = existing_pdf.getPage(0)
    page.mergePage(new_pdf.getPage(0))
    output.addPage(page)
    outputStream = open(f"assets/temp/Surat Kelulusan {nama}.pdf", "wb")
    output.write(outputStream)
    outputStream.close()
    return True

def generateSuratJPAStimlog(nama, nisn, password, nomor_va):
    packet = io.BytesIO()
    can = canvas.Canvas(packet, pagesize=A4)
    can.setFont('Times-Bold', 10)
    can.drawString(208, 536, nama)
    can.drawString(208, 510, nisn)
    can.drawString(208, 483, password)
    can.drawString(208, 457, nomor_va)
    can.save()
    packet.seek(0)
    new_pdf = PdfFileReader(packet)
    existing_pdf = PdfFileReader(open("assets/template/Template Surat Undangan JPA Stimlog.pdf", "rb"))
    output = PdfFileWriter()
    page = existing_pdf.getPage(0)
    page.mergePage(new_pdf.getPage(0))
    output.addPage(page)
    outputStream = open(f"assets/temp/Surat Kelulusan {nama}.pdf", "wb")
    output.write(outputStream)
    outputStream.close()
    return True

def generateSertifikat(nama, pathfile):
    pdfmetrics.registerFont(TTFont('Great Vibes', 'assets/template/tulisan/GreatVibes.ttf'))
    packet = io.BytesIO()
    can = canvas.Canvas(packet, pagesize=A4)
    can.setFont('Great Vibes', 36)
    can.drawCentredString(310, 343, nama)
    can.save()
    packet.seek(0)
    new_pdf = PdfFileReader(packet)
    existing_pdf = PdfFileReader(open(f"assets/template/sertifikat/{pathfile}", "rb"))
    output = PdfFileWriter()
    page = existing_pdf.getPage(0)
    page.mergePage(new_pdf.getPage(0))
    output.addPage(page)
    outputStream = open(f"assets/temp/E-Certificate {nama}.pdf", "wb")
    output.write(outputStream)
    outputStream.close()
    return True

def generateSuratEventUndanganKhusus(nama, nama_surat, pathfile):
    packet = io.BytesIO()
    can = canvas.Canvas(packet, pagesize=A4)
    can.setFont('Times-Bold', 10)
    can.drawString(88, 645, nama)
    can.save()
    packet.seek(0)
    new_pdf = PdfFileReader(packet)
    existing_pdf = PdfFileReader(open(f"assets/template/sertifikat/{pathfile}", "rb"))
    output = PdfFileWriter()
    page = existing_pdf.getPage(0)
    page.mergePage(new_pdf.getPage(0))
    output.addPage(page)
    outputStream = open(f"assets/temp/{nama_surat} {nama}.pdf", "wb")
    output.write(outputStream)
    outputStream.close()
    return True
