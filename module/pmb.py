from helpers.message import *
from helpers.database import *
import pandas as pd
import numpy as np
import uuid
import base64

# import magic
# mime = magic.Magic(mime=True)
import numpy as np
import time
from module.googleContact import *
from module.workeremail import *
from module.workerpdf import *
import pandas as pd
import os


def masukinDataPMB(message_from, message_body):
    data = pd.read_excel(f"assets/userfile/{message_from}{message_body}")
    df = pd.DataFrame(
        data,
        columns=[
            "First Name",
            "Company",
            "Mobile Phone",
            "Email Address",
            "Jenis",
            "Jalur",
        ],
    )
    df = df.replace({np.nan: None})
    inserted_data = 0
    tmp_nama = []
    tmp_sma = []
    tmp_nomor = []
    tmp_email = []
    tmp_jenis = []
    tmp_jalur = []
    for data in df.values.tolist():
        if str(data[2]).startswith("8"):
            data[2] = "62" + str(data[2])
        if ".0" in str(data[2]):
            data[2] = str(data[2]).replace(".0", "")
        if not cek_nomor_telepon(str(data[2])):
            tmp_nama.append(data[0])
            tmp_sma.append(data[1])
            tmp_nomor.append(data[2])
            tmp_email.append(data[3])
            tmp_jalur.append(data[4])
            tmp_jenis.append(data[5])
        else:
            if cekDataSudahAda(data[0].replace("21-JU ", "")):
                pass
            else:
                if str(data[2]).startswith("0"):
                    data[2] = "62" + str(data[2][1:])
                if insertDataPMB(
                    data[0].replace("21-JU ", ""),
                    data[1],
                    data[2],
                    data[3],
                    data[4],
                    data[5],
                ):
                    listGoogleContact.append(
                        {"nama": data[0], "nomor_telepon": data[2]}
                    )
                    inserted_data += 1
    if tmp_nama:
        uid = uuid.uuid4()
        pd.DataFrame(
            {
                "First Name": tmp_nama,
                "Company": tmp_sma,
                "Mobile Phone": tmp_nomor,
                "Email Address": tmp_email,
                "Jenis": tmp_jenis,
                "Jalur": tmp_jalur,
            }
        ).to_excel(f"assets/temp/{uid}.xlsx")
    else:
        uid = None
    return inserted_data, uid


def masukinDataPMBJPA(message_from, message_body):
    data = pd.read_excel(f"assets/userfile/{message_from}{message_body}")
    df = pd.DataFrame(
        data,
        columns=[
            "Nama Lengkap",
            "NISN",
            "Password",
            "Jenis Kelamin",
            "No Telepon",
            "Email",
            "Perguruan Tinggi",
            "Jalur",
            "No Transaksi",
            "Virtual Account",
            "Jumlah Tagihan",
            "Tgl Expired VA",
            "Status Pembayaran",
            "Tanggal Daftar",
        ],
    )
    df = df.replace({np.nan: None})
    inserted_data = 0
    tmp = []
    for data in df.values.tolist():
        data[13] = datetime.datetime.strptime(data[13], "%d %B %Y").strftime("%Y-%m-%d")
        if str(data[4]).startswith("8"):
            data[4] = "62" + str(data[4])
        if ".0" in str(data[4]):
            data[4] = str(data[4]).replace(".0", "")
        if not cek_nomor_telepon(str(data[4])):
            tmp.append(data)
        else:
            if cekDataSudahAdaJPA(data[0]):
                pass
            else:
                if str(data[4]).startswith("0"):
                    data[4] = "62" + str(data[4][1:])
                if insertDataPMBJPA(
                    data[0],
                    data[1],
                    data[2],
                    data[3],
                    data[4],
                    data[5],
                    data[6],
                    data[7],
                    data[8],
                    data[9],
                    data[10],
                    data[11],
                    data[12],
                    data[13],
                ):
                    listGoogleContact.append(
                        {"nama": data[0], "nomor_telepon": data[4]}
                    )
                    inserted_data += 1
        if tmp:
            uid = uuid.uuid4()
            pd.DataFrame(
                {
                    "Nama Lengkap": tmp[0],
                    "NISN": tmp[1],
                    "Password": tmp[2],
                    "Jenis Kelamin": tmp[3],
                    "No Telepon": tmp[4],
                    "Email": tmp[5],
                    "Perguruan Tinggi": tmp[6],
                    "Jalur": tmp[7],
                    "No Transaksi": tmp[8],
                    "Virtual Account": tmp[9],
                    "Jumlah Tagihan": tmp[10],
                    "Tgl Expired VA": tmp[11],
                    "Status Pembayaran": tmp[12],
                    "Tanggal Daftar": tmp[13],
                }
            ).to_excel(f"assets/temp/{uid}.xlsx")
        else:
            uid = None
    return inserted_data, uid


def masukinDataPMBSertifikat(filepath):
    data = pd.read_excel(f"assets/userfile/{filepath}")
    df = pd.DataFrame(
        data,
        columns=[
            "First Name",
            "Company",
            "Mobile Phone",
            "Email Address",
            "Jenis",
            "Jalur",
        ],
    )
    df = df.replace({np.nan: None})
    inserted_data = 0
    tmp_nama = []
    tmp_sma = []
    tmp_nomor = []
    tmp_email = []
    tmp_jenis = []
    tmp_jalur = []
    for data in df.values.tolist():
        if str(data[2]).startswith("8"):
            data[2] = "62" + str(data[2])
        if ".0" in str(data[2]):
            data[2] = str(data[2]).replace(".0", "")
        if not cek_nomor_telepon(str(data[2])):
            tmp_nama.append(data[0])
            tmp_sma.append(data[1])
            tmp_nomor.append(data[2])
            tmp_email.append(data[3])
            tmp_jalur.append(data[4])
            tmp_jenis.append(data[5])
        else:
            if str(data[2]).startswith("0"):
                data[2] = "62" + str(data[2][1:])
            if insertDataPMBSertifikat(
                data[0].replace("21-JU ", ""),
                data[1],
                data[2],
                data[3],
                data[4],
                data[5],
            ):
                listGoogleContact.append({"nama": data[0], "nomor_telepon": data[2]})
                inserted_data += 1
    if tmp_nama:
        uid = uuid.uuid4()
        pd.DataFrame(
            {
                "First Name": tmp_nama,
                "Company": tmp_sma,
                "Mobile Phone": tmp_nomor,
                "Email Address": tmp_email,
                "Jenis": tmp_jenis,
                "Jalur": tmp_jalur,
            }
        ).to_excel(f"assets/temp/{uid}.xlsx")
    else:
        uid = None
    return inserted_data, uid


def dataPesan():
    db = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    logs_pesan = pd.read_sql("SELECT nomor_telepon, status FROM logs_pesan", con=db)
    data_undangan = pd.read_sql("SELECT nomor_telepon, respond FROM pmb", con=db)
    data_all = pd.merge(logs_pesan, data_undangan, on="nomor_telepon")
    data_all_status = data_all.groupby("status")["nomor_telepon"].nunique()
    respon = data_all.groupby("respond")["nomor_telepon"].nunique()[1]
    pending = data_all_status["Pending"]
    terkirim = data_all_status["Terkirim"]
    sudah_dibaca = data_all_status["Sudah Dibaca"]
    belum_dikirim = len(
        data_undangan[
            ~data_undangan["nomor_telepon"].isin(logs_pesan["nomor_telepon"].tolist())
        ]["nomor_telepon"]
    )
    data_semua = belum_dikirim + pending + terkirim + sudah_dibaca
    return {
        "percent_belum_dikirim": dataPercent(belum_dikirim, data_semua),
        "percent_pending": dataPercent(pending, data_semua),
        "percent_terkirim": dataPercent(terkirim, data_semua),
        "percent_sudah_dibaca": dataPercent(sudah_dibaca, data_semua),
        "percent_respon": dataPercent(respon, data_semua),
        "belum_dikirim": belum_dikirim,
        "pending": pending,
        "terkirim": terkirim,
        "sudah_dibaca": sudah_dibaca,
        "respon": respon,
        "data_semua": data_semua,
    }


def dataPesanJPA():
    db = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    logs_pesan = pd.read_sql("SELECT nomor_telepon, status FROM logs_pesan", con=db)
    logs = pd.read_sql("SELECT nomor_telepon FROM logs", con=db).nomor_telepon.unique()
    data_undangan = pd.read_sql(
        "SELECT no_telepon AS nomor_telepon FROM pmb_jpa WHERE jalur = 'JPA'", con=db
    )
    data_all = pd.merge(logs_pesan, data_undangan, on="nomor_telepon")
    data_all_status = data_all.groupby("status")["nomor_telepon"].nunique()
    belum_dikirim = len(
        data_undangan[
            ~data_undangan["nomor_telepon"].isin(logs_pesan["nomor_telepon"].tolist())
        ]["nomor_telepon"]
    )
    respon = len(data_undangan[data_undangan["nomor_telepon"].isin(logs)])
    try:
        pending = data_all_status["Pending"]
    except:
        pending = 0
    try:
        terkirim = data_all_status["Terkirim"]
    except:
        terkirim = 0
    try:
        sudah_dibaca = data_all_status["Sudah Dibaca"]
    except:
        sudah_dibaca = 0
    data_semua = belum_dikirim + pending + terkirim + sudah_dibaca
    return {
        "percent_belum_dikirim": dataPercent(belum_dikirim, data_semua),
        "percent_pending": dataPercent(pending, data_semua),
        "percent_terkirim": dataPercent(terkirim, data_semua),
        "percent_sudah_dibaca": dataPercent(sudah_dibaca, data_semua),
        "percent_respon": dataPercent(respon, data_semua),
        "belum_dikirim": belum_dikirim,
        "pending": pending,
        "terkirim": terkirim,
        "sudah_dibaca": sudah_dibaca,
        "respon": respon,
        "data_semua": data_semua,
    }


def dataPesanEvent():
    db = pymysql.connect(
        host=config.db_host,
        user=config.db_username,
        passwd=config.db_password,
        db=config.db_name,
        port=config.db_port,
    )
    logs_pesan = pd.read_sql("SELECT nomor_telepon, status FROM logs_pesan", con=db)
    logs = pd.read_sql("SELECT nomor_telepon FROM logs", con=db).nomor_telepon.unique()
    data_event = pd.read_sql(
        "SELECT nomor_telepon, pengiriman FROM pmb_sertifikat", con=db
    )
    data_all = pd.merge(logs_pesan, data_event, on="nomor_telepon")
    data_all_status = data_all.groupby("status")["nomor_telepon"].nunique()
    try:
        respon = len(data_event[data_event["nomor_telepon"].isin(logs)])
    except:
        respon = 0
    try:
        pending = data_all_status["Pending"]
    except:
        pending = 0
    try:
        terkirim = data_all_status["Terkirim"]
        terkirim_email = len(data_event[data_event["pengiriman"] == 1])
    except:
        terkirim = 0
        terkirim_email = 0
    try:
        sudah_dibaca = data_all_status["Sudah Dibaca"]
    except:
        sudah_dibaca = 0
    # belum_dikirim = len(data_event[~data_event["nomor_telepon"].isin(logs_pesan["nomor_telepon"].tolist())]["nomor_telepon"])
    belum_dikirim = len(data_event[data_event["pengiriman"] == 0])
    data_semua = belum_dikirim + pending + terkirim + terkirim_email + sudah_dibaca
    return {
        "percent_belum_dikirim": dataPercent(belum_dikirim, data_semua),
        "percent_pending": dataPercent(pending, data_semua),
        "percent_terkirim": dataPercent(terkirim, data_semua),
        "percent_terkirim_email": dataPercent(terkirim_email, data_semua),
        "percent_sudah_dibaca": dataPercent(sudah_dibaca, data_semua),
        "percent_respon": dataPercent(respon, data_semua),
        "belum_dikirim": belum_dikirim,
        "pending": pending,
        "terkirim": terkirim,
        "terkirim_email": terkirim_email,
        "sudah_dibaca": sudah_dibaca,
        "respon": respon,
        "data_semua": data_semua,
    }


def encodeFileData(nama_file):
    with open(nama_file, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read()).decode()
        mime_type = mime.from_file(nama_file)
        return nama_file.replace("assets/template/", ""), encoded_string, mime_type


def kirimUndangan(nama, asal_sekolah, nomor_telepon, email):
    pesan = getTemplatePesan("undangan")
    if checkLogsPengiriman(nomor_telepon):
        return False
    else:
        if waKirimPesan(
            nomor_telepon,
            pesan.replace("#NAMA#", nama)
            .replace("#SEKOLAH#", asal_sekolah)
            .replace("#EMAIL#", email),
        ):
            print(f"Pengiriman pesan WA ke: {nomor_telepon}")
            return True
        else:
            return False


def kirimUndanganJPA(nama, NISN, nomor_telepon, email):
    pesan = getTemplatePesan("jpa")
    if checkLogsPengiriman(nomor_telepon):
        print(f"Nomor {nomor_telepon} Sudah Dikirim")
        return False
    else:
        if waKirimPesan(
            nomor_telepon,
            pesan.replace("#NAMA#", nama)
            .replace("#NISN#", NISN)
            .replace("#EMAIL#", email),
        ):
            print(f"Pengiriman pesan WA ke: {nomor_telepon}")
            return True
        else:
            print(f"ERROR: Pengiriman pesan WA ke: {nomor_telepon}")
            return False


def kirimEvent(nama, asal_sekolah, nomor_telepon, email):
    pesan = getTemplatePesan("event")
    if checkLogsPengiriman(nomor_telepon):
        return False
    else:
        if waKirimPesan(
            nomor_telepon,
            pesan.replace("#NAMA#", nama)
            .replace("#SEKOLAH#", asal_sekolah)
            .replace("#EMAIL#", email),
        ):
            print(f"Pengiriman pesan WA ke: {nomor_telepon}")
            return True
        else:
            return False


listJob = []
jenisJob = ""
listGoogleContact = []


def executePesan():
    print("Job Diterima")
    if jenisJob == "undangan":
        for x in range(0, len(listJob)):
            timer = np.random.randint(7, 25)
            data = listJob[0]
            if kirimUndangan(
                data["nama"], data["asal_sekolah"], data["nomor_telepon"], data["email"]
            ):
                generateSuratUndanganPoltekpos(data["nama"], data["asal_sekolah"])
                kirimEmailPoltekposUndangan(
                    data["email"], f"assets/temp/Surat Kelulusan {data['nama']}.pdf"
                )
                generateSuratUndanganStimlog(data["nama"], data["asal_sekolah"])
                kirimEmailStimlogUndangan(
                    data["email"], f"assets/temp/Surat Kelulusan {data['nama']}.pdf"
                )
                print(data["nama"])
                listJob.pop(0)
                print(
                    f"Waktu Pengiriman selanjutnya: {timer} detik sisa {len(listJob)}"
                )
                time.sleep(timer)
            else:
                listJob.pop(0)
    elif jenisJob == "jpa":
        print("Job JPA Diterima")
        for x in range(0, len(listJob)):
            timer = np.random.randint(7, 25)
            data = listJob[0]
            if kirimUndanganJPA(
                data["nama_lengkap"], data["nisn"], data["no_telepon"], data["email"]
            ):
                if data["perguruan_tinggi"] == "POLTEKPOS":
                    generateSuratJPAPoltepos(
                        data["nama_lengkap"],
                        data["nisn"],
                        data["password"],
                        data["virtual_account"],
                    )
                    kirimEmailPoltekposUndanganJPA(
                        data["email"],
                        data["nama_lengkap"],
                        data["virtual_account"],
                        data["nisn"],
                        data["password"],
                        f"assets/temp/Surat Kelulusan {data['nama_lengkap']}.pdf",
                    )
                    listJob.pop(0)
                    print(
                        f"Waktu Pengiriman selanjutnya: {timer} detik sisa {len(listJob)}"
                    )
                    time.sleep(timer)
                elif data["perguruan_tinggi"] == "STIMLOG":
                    generateSuratJPAStimlog(
                        data["nama_lengkap"],
                        data["nisn"],
                        data["password"],
                        data["virtual_account"],
                    )
                    kirimEmailStimlogUndanganJPA(
                        data["email"],
                        data["nama_lengkap"],
                        data["virtual_account"],
                        data["nisn"],
                        data["password"],
                        f"assets/temp/Surat Kelulusan {data['nama_lengkap']}.pdf",
                    )
                    listJob.pop(0)
                    print(
                        f"Waktu Pengiriman selanjutnya: {timer} detik sisa {len(listJob)}"
                    )
                    time.sleep(timer)
            else:
                listJob.pop(0)
    elif jenisJob == "undangansertifikat":
        print("Job Event Undangan Sertifikat Diterima")
        for x in range(0, len(listJob)):
            timer = np.random.randint(7, 25)
            data = listJob[0]
            if kirimEvent(
                data["nama"], data["asal_sekolah"], data["nomor_telepon"], data["email"]
            ):
                generateSertifikat(data["nama"], data["filepath"])
                generateSuratUndanganPoltekpos(data["nama"], data["asal_sekolah"])
                kirimEmailPoltekposUndanganSertifikat(
                    data["email"],
                    data["nama_event"],
                    data["tanggal"],
                    f"assets/temp/Surat Kelulusan {data['nama']}.pdf",
                    f"assets/temp/E-Certificate {data['nama']}.pdf",
                )
                generateSuratUndanganStimlog(data["nama"], data["asal_sekolah"])
                kirimEmailStimlogUndanganSertifikat(
                    data["email"],
                    data["nama_event"],
                    data["tanggal"],
                    f"assets/temp/Surat Kelulusan {data['nama']}.pdf",
                    f"assets/temp/E-Certificate {data['nama']}.pdf",
                )
                listJob.pop(0)
                updatePengirimanSertifikat(data["id"])
                print(
                    f"Waktu Pengiriman selanjutnya: {timer} detik sisa {len(listJob)}"
                )
                time.sleep(timer)
            else:
                listJob.pop(0)
    elif jenisJob == "khusussertifikatemail":
        print("Job Event Sertifikat Email Diterima")
        for x in range(0, len(listJob)):
            timer = np.random.randint(7, 25)
            data = listJob[0]
            generateSertifikat(data["nama"], data["filepath"])
            try:
                print(
                    kirimEmailCustomSertifikat(
                        "poltekpos",
                        data["email"],
                        data["nama_event"],
                        data["attachment"],
                        data["nama_event"],
                        data["tanggal"],
                        data["path_email"],
                        f"E-Certificate {data['nama']}.pdf",
                        f"assets/temp/E-Certificate {data['nama']}.pdf",
                    )
                )
                print(
                    kirimEmailCustomSertifikat(
                        "stimlog",
                        data["email"],
                        data["nama_event"],
                        data["attachment"],
                        data["nama_event"],
                        data["tanggal"],
                        data["path_email"],
                        f"E-Certificate {data['nama']}.pdf",
                        f"assets/temp/E-Certificate {data['nama']}.pdf",
                    )
                )
                updatePengirimanSertifikat(data["id"])
            except Exception as e:
                print(e)
            listJob.pop(0)
    elif jenisJob == "khususemailstimlog":
        print("Job Event Blast Email Stimlog Diterima")
        for x in range(0, len(listJob)):
            timer = np.random.randint(7, 25)
            data = listJob[0]
            try:
                if kirimEmailBlastCustom(
                    "stimlog",
                    data["email"],
                    data["nama_event"],
                    data["attachment"],
                    data["nama_event"],
                    data["tanggal"],
                    data["isi_email"],
                ):
                    updatePengirimanSertifikat(data["id"])
            except Exception as e:
                print(e)
            listJob.pop(0)
    elif jenisJob == "khususemailpoltekpos":
        print("Job Event Blast Email Poltekpos Diterima")
        for x in range(0, len(listJob)):
            timer = np.random.randint(7, 25)
            data = listJob[0]
            try:
                if kirimEmailBlastCustom(
                    "poltekpos",
                    data["email"],
                    data["nama_event"],
                    data["attachment"],
                    data["nama_event"],
                    data["tanggal"],
                    data["isi_email"],
                ):
                    updatePengirimanSertifikat(data["id"])
            except Exception as e:
                print(e)
            listJob.pop(0)
    elif jenisJob == "khususemailstimlogPDF":
        print("Job Event Blast Email Stimlog PDF Diterima")
        for x in range(0, len(listJob)):
            timer = np.random.randint(7, 25)
            data = listJob[0]
            try:
                if generateSuratEventUndanganKhusus(
                    data["nama"], data["attachment"], data["filepath"]
                ):
                    if kirimEmailBlastCustomPDF(
                        "stimlog",
                        data["email"],
                        data["nama_event"],
                        f'assets/temp/{data["attachment"]} {data["nama"]}.pdf',
                        data["nama_event"],
                        data["tanggal"],
                        data["isi_email"],
                    ):
                        updatePengirimanSertifikat(data["id"])
                else:
                    print("ERROR")
                    listJob.pop(0)
            except Exception as e:
                print(e)
            listJob.pop(0)
    elif jenisJob == "khususemailpoltekposPDF":
        print("Job Event Blast Email Poltekpos PDF Diterima")
        for x in range(0, len(listJob)):
            timer = np.random.randint(7, 25)
            data = listJob[0]
            try:
                if generateSuratEventUndanganKhusus(
                    data["nama"], data["attachment"], data["filepath"]
                ):
                    if kirimEmailBlastCustomPDF(
                        "poltekpos",
                        data["email"],
                        data["nama_event"],
                        f'assets/temp/{data["attachment"]} {data["nama"]}.pdf',
                        data["nama_event"],
                        data["tanggal"],
                        data["isi_email"],
                    ):
                        updatePengirimanSertifikat(data["id"])
                else:
                    print("ERROR")
                    listJob.pop(0)
            except Exception as e:
                print(e)
            listJob.pop(0)
    else:
        print("Tidak ada jenis Job")
    print("Job Selesai")


def executeTambahContact():
    print("Job tambah kontak diterima")
    for x in range(0, len(listGoogleContact)):
        data = listGoogleContact[0]
        if createContact(data["nama"], str(data["nomor_telepon"])):
            listGoogleContact.pop(0)
        else:
            listGoogleContact.append(listGoogleContact[0])
            listGoogleContact.pop(0)
            print("Error")
    if len(listGoogleContact) > 0:
        data = listGoogleContact[0]
        if createContact(data["nama"], str(data["nomor_telepon"])):
            listGoogleContact.pop(0)
        else:
            listGoogleContact.append(listGoogleContact[0])
            listGoogleContact.pop(0)
            print("Error")
    print("Job tambah kontak selesai")
