import smtplib
import ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.image import MIMEImage
from email.mime.base import MIMEBase
from email import encoders
from module.pmb import *

def kirimEmail(penerima, subject , body):
    try:
        sender_email = config.email_alerts
        sender_pass = config.email_alerts_password
        message = MIMEMultipart()
        message["From"] = f'PMB <{config.email_alerts}>'
        message["To"] = penerima
        message["Subject"] = subject
        message["Bcc"] = penerima
        message.attach(MIMEText(body, "plain"))
        text = message.as_string()
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
            server.login(sender_email, sender_pass)
            server.sendmail(sender_email, penerima, text)
        print(f'Email berhasil dikirim ke {penerima}')
        return True
    except Exception as e: 
        print(str(e))
        return False

#############################################################################################
# Undangan
#############################################################################################
def kirimEmailPoltekposUndangan(penerima, file):
    try:
        sender_email = config.email_alerts
        sender_pass = config.email_alerts_password
        message = MIMEMultipart()
        message["From"] = f'PMB Politeknik Pos Indonesia<{config.email_alerts}>'
        message["To"] = penerima
        message["Subject"] = "Surat Kelulusan Jalur Undangan POLTEKPOS"
        message["Bcc"] = penerima
        attachment = "assets/template/poltekpos.png"
        html = open("assets/template/email/email_poltekpos_undangan.html")
        message.attach(MIMEText(html.read().replace("{attachment}", attachment), "html"))
        #message.attach(MIMEText(body, "plain"))
        fp = open(attachment, 'rb')                                                    
        img = MIMEImage(fp.read())
        fp.close()
        img.add_header('Content-ID', '<{}>'.format(attachment))
        message.attach(img)
        with open(f'{file}', "rb") as fileattachment:
            part = MIMEBase("application", "octet-stream")
            part.set_payload(fileattachment.read())
        encoders.encode_base64(part)
        part.add_header(
            "Content-Disposition",
            "attachment; filename= %s " % file.split("/")[-1:][0],
        )
        message.attach(part)
        text = message.as_string()
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
            server.login(sender_email, sender_pass)
            server.sendmail(sender_email, penerima, text)
        print(f'Email Poltekpos Undangan berhasil dikirim ke {penerima}')
        return True
    except Exception as e: 
        print(str(e))
        return False

def kirimEmailStimlogUndangan(penerima, file):
    try:
        sender_email = config.email_alerts_stimlog
        sender_pass = config.email_alerts_stimlog_password
        message = MIMEMultipart()
        message["From"] = f'PMB STIMLOG<{config.email_alerts_stimlog}>'
        message["To"] = penerima
        message["Subject"] = "Surat Kelulusan Jalur Undangan STIMLOG"
        message["Bcc"] = penerima
        attachment = "assets/template/stimlog.png"
        html = open("assets/template/email/email_stimlog_undangan.html")
        message.attach(MIMEText(html.read().replace("{attachment}", attachment), "html"))
        #message.attach(MIMEText(body, "plain"))
        fp = open(attachment, 'rb')                                                    
        img = MIMEImage(fp.read())
        fp.close()
        img.add_header('Content-ID', '<{}>'.format(attachment))
        message.attach(img)
        with open(f'{file}', "rb") as fileattachment:
            part = MIMEBase("application", "octet-stream")
            part.set_payload(fileattachment.read())
        encoders.encode_base64(part)
        part.add_header(
            "Content-Disposition",
            "attachment; filename= %s " % file.split("/")[-1:][0],
        )
        message.attach(part)
        text = message.as_string()
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
            server.login(sender_email, sender_pass)
            server.sendmail(sender_email, penerima, text)
        print(f'Email Stimlog Undangan berhasil dikirim ke {penerima}')
        return True
    except Exception as e: 
        print(str(e))
        return False


#############################################################################################
# Undangan dan Sertifikat
#############################################################################################
def kirimEmailPoltekposUndanganSertifikat(penerima, nama_event, tanggal_event, file_undangan, file_sertifikat):
    try:
        sender_email = config.email_alerts
        sender_pass = config.email_alerts_password
        message = MIMEMultipart()
        message["From"] = f'PMB Politeknik Pos Indonesia<{config.email_alerts}>'
        message["To"] = penerima
        message["Subject"] = "Pemberitahuan"
        message["Bcc"] = penerima
        attachment = "assets/template/poltekpos.png"
        html = open("assets/template/email/email_poltekpos_undangan_sertifikat.html")
        message.attach(MIMEText(html.read().replace("{attachment}", attachment).replace("#NAMA_EVENT#", nama_event).replace("#TANGGAL#", tanggal_event), "html"))
        #message.attach(MIMEText(body, "plain"))
        fp = open(attachment, 'rb')                                                    
        img = MIMEImage(fp.read())
        fp.close()
        img.add_header('Content-ID', '<{}>'.format(attachment))
        message.attach(img)
        #Attachment file awal
        with open(f'{file_undangan}', "rb") as fileattachment:
            part = MIMEBase("application", "octet-stream")
            part.set_payload(fileattachment.read())
        encoders.encode_base64(part)
        part.add_header("Content-Disposition",
            "attachment; filename= %s " % file_undangan.split("/")[-1:][0].replace("Kelulusan", "Jalur Undangan"),
        )
        message.attach(part)
        #Attachment file akhir
        #Attachment file awal
        with open(f'{file_sertifikat}', "rb") as fileattachment:
            part2 = MIMEBase("application", "octet-stream")
            part2.set_payload(fileattachment.read())
        encoders.encode_base64(part2)
        part2.add_header("Content-Disposition",
            "attachment; filename= %s " % file_sertifikat.split("/")[-1:][0],
        )
        message.attach(part2)
        #Attachment file akhir
        text = message.as_string()
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
            server.login(sender_email, sender_pass)
            server.sendmail(sender_email, penerima, text)
        print(f'Email Poltekpos Undangan Sertifikat berhasil dikirim ke {penerima}')
        return True
    except Exception as e: 
        print(str(e))
        return False

def kirimEmailStimlogUndanganSertifikat(penerima, nama_event, tanggal_event, file_undangan, file_sertifikat):
    try:
        sender_email = config.email_alerts_stimlog
        sender_pass = config.email_alerts_stimlog_password
        message = MIMEMultipart()
        message["From"] = f'PMB STIMLOG<{config.email_alerts_stimlog}>'
        message["To"] = penerima
        message["Subject"] = "Pemberitahuan"
        message["Bcc"] = penerima
        attachment = "assets/template/stimlog.png"
        html = open("assets/template/email/email_stimlog_undangan_sertifikat.html")
        message.attach(MIMEText(html.read().replace("{attachment}", attachment).replace("#NAMA_EVENT#", nama_event).replace("#TANGGAL#", tanggal_event), "html"))
        #message.attach(MIMEText(body, "plain"))
        fp = open(attachment, 'rb')                                                    
        img = MIMEImage(fp.read())
        fp.close()
        img.add_header('Content-ID', '<{}>'.format(attachment))
        message.attach(img)
        #Attachment file awal
        with open(f'{file_undangan}', "rb") as fileattachment:
            part = MIMEBase("application", "octet-stream")
            part.set_payload(fileattachment.read())
        encoders.encode_base64(part)
        part.add_header("Content-Disposition",
            "attachment; filename= %s " % file_undangan.split("/")[-1:][0].replace("Kelulusan", "Jalur Undangan"),
        )
        message.attach(part)
        #Attachment file akhir
        #Attachment file awal
        with open(f'{file_sertifikat}', "rb") as fileattachment:
            part2 = MIMEBase("application", "octet-stream")
            part2.set_payload(fileattachment.read())
        encoders.encode_base64(part2)
        part2.add_header("Content-Disposition",
            "attachment; filename= %s " % file_sertifikat.split("/")[-1:][0],
        )
        message.attach(part2)
        #Attachment file akhir
        text = message.as_string()
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
            server.login(sender_email, sender_pass)
            server.sendmail(sender_email, penerima, text)
        print(f'Email Stimlog Undangan Sertifikat berhasil dikirim ke {penerima}')
        return True
    except Exception as e: 
        print(str(e))
        return False

#############################################################################################
# Kirim Email Custom untuk Sertifikat
#############################################################################################
def kirimEmailCustomSertifikat(kampus, penerima, subjek, attachment,nama_event, tanggal_event, lokasi_template_email ,nama_file, filepath):
    if kampus == "poltekpos":
        sender_email = config.email_alerts
        sender_pass = config.email_alerts_password
        message = MIMEMultipart()
        message["From"] = f'PMB Politeknik Pos Indonesia<{sender_email}>'
        message["Subject"] = subjek
        html = open(f"assets/template/email/poltekpos_{lokasi_template_email}")
    elif kampus == "stimlog":
        sender_email = config.email_alerts_stimlog
        sender_pass = config.email_alerts_stimlog_password
        message = MIMEMultipart()
        message["From"] = f'PMB Stimlog<{sender_email}>'
        message["Subject"] = subjek
        html = open(f"assets/template/email/stimlog_{lokasi_template_email}")
    try:
        message["To"] = penerima
        message["Bcc"] = penerima
        message.attach(MIMEText(html.read().replace("{attachment}", attachment).replace("#NAMA_EVENT#", nama_event).replace("#TANGGAL#", tanggal_event), "html"))
        #message.attach(MIMEText(body, "plain"))
        fp = open(attachment, 'rb')                                                    
        img = MIMEImage(fp.read())
        fp.close()
        img.add_header('Content-ID', '<{}>'.format(attachment))
        message.attach(img)
        #Attachment file awal
        with open(f'{filepath}', "rb") as fileattachment:
            part = MIMEBase("application", "octet-stream")
            part.set_payload(fileattachment.read())
        encoders.encode_base64(part)
        part.add_header("Content-Disposition",
            "attachment; filename= %s " % nama_file,
        )
        message.attach(part)
        #Attachment file akhir
        text = message.as_string()
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
            server.login(sender_email, sender_pass)
            server.sendmail(sender_email, penerima, text)
        print(f'Email {kampus} Custom Sertifikat berhasil dikirim ke {penerima}')
        return True
    except Exception as e: 
        print(str(e))
        return False

def kirimEmailBlastCustom(kampus, penerima, subjek, attachment, nama_event, tanggal_event, isi_email):
    if kampus == "poltekpos":
        sender_email = config.email_alerts
        sender_pass = config.email_alerts_password
        message = MIMEMultipart()
        message["From"] = f'PMB Politeknik Pos Indonesia<{sender_email}>'
        message["Subject"] = subjek
        html = open(f"assets/template/email/template/poltekpos.html").read()
    elif kampus == "stimlog":
        sender_email = config.email_alerts_stimlog
        sender_pass = config.email_alerts_stimlog_password
        message = MIMEMultipart()
        message["From"] = f'PMB Stimlog<{sender_email}>'
        message["Subject"] = subjek
        html = open("assets/template/email/template/stimlog.html").read()
    try:
        message["To"] = penerima
        message["Bcc"] = penerima
        message.attach(MIMEText(html.replace("#ISIEMAIL#", isi_email), "html"))
        text = message.as_string()
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
            server.login(sender_email, sender_pass)
            server.sendmail(sender_email, penerima, text)
        print(f'Email Custom Sertifikat berhasil dikirim ke {penerima}')
        return True
    except Exception as e: 
        print(str(e))
        return False

def kirimEmailBlastCustomPDF(kampus, penerima, subjek, attachment, nama_event, tanggal_event, isi_email):
    if kampus == "poltekpos":
        sender_email = config.email_alerts
        sender_pass = config.email_alerts_password
        message = MIMEMultipart()
        message["From"] = f'PMB Politeknik Pos Indonesia<{sender_email}>'
        message["Subject"] = subjek
        html = open(f"assets/template/email/template/poltekpos.html").read()
    elif kampus == "stimlog":
        sender_email = config.email_alerts_stimlog
        sender_pass = config.email_alerts_stimlog_password
        message = MIMEMultipart()
        message["From"] = f'PMB Stimlog<{sender_email}>'
        message["Subject"] = subjek
        html = open("assets/template/email/template/stimlog.html").read()
    try:
        message["To"] = penerima
        message["Bcc"] = penerima
        message.attach(MIMEText(html.replace("#ISIEMAIL#", isi_email), "html"))
        #Attachment file awal
        with open(attachment, "rb") as fileattachment:
            part = MIMEBase("application", "octet-stream")
            part.set_payload(fileattachment.read())
        encoders.encode_base64(part)
        part.add_header("Content-Disposition",
            "attachment; filename= %s " % attachment.split("/")[2],
        )
        message.attach(part)
        #Attachment file akhir
        text = message.as_string()
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
            server.login(sender_email, sender_pass)
            server.sendmail(sender_email, penerima, text)
        print(f'Email Custom Sertifikat berhasil dikirim ke {penerima}')
        return True
    except Exception as e: 
        print(str(e))
        return False

#############################################################################################
# Jalur Prestasi Akademik
#############################################################################################
def kirimEmailStimlogUndanganJPA(penerima, nama, nomor_va, username, password, file):
    try:
        sender_email = config.email_alerts_stimlog
        sender_pass = config.email_alerts_stimlog_password
        message = MIMEMultipart()
        message["From"] = f'PMB STIMLOG<{config.email_alerts_stimlog}>'
        message["To"] = penerima
        message["Subject"] = "Surat Kelulusan Jalur Rapor STIMLOG"
        message["Bcc"] = penerima
        attachment = "assets/template/stimlog.png"
        html = open("assets/template/email/email_stimlog.html")
        message.attach(MIMEText(html.read().replace("{attachment}", attachment).replace("{USERNAME}", username).replace("{PASSWORD}", password).replace("{NAMA}", nama).replace("{NOMOR_VA}", nomor_va), "html"))
        #message.attach(MIMEText(body, "plain"))
        fp = open(attachment, 'rb')                                                    
        img = MIMEImage(fp.read())
        fp.close()
        img.add_header('Content-ID', '<{}>'.format(attachment))
        message.attach(img)
        with open(f'{file}', "rb") as fileattachment:
            part = MIMEBase("application", "octet-stream")
            part.set_payload(fileattachment.read())
        encoders.encode_base64(part)
        part.add_header(
            "Content-Disposition",
            "attachment; filename= %s " % file.split("/")[-1:][0],
        )
        message.attach(part)
        text = message.as_string()
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
            server.login(sender_email, sender_pass)
            server.sendmail(sender_email, penerima, text)
        print(f'Email Stimlog JPA berhasil dikirim ke {penerima}')
        return True
    except Exception as e: 
        print(str(e))
        return False

def kirimEmailPoltekposUndanganJPA(penerima, nama, nomor_va, username, password, file):
    try:
        sender_email = config.email_alerts
        sender_pass = config.email_alerts_password
        message = MIMEMultipart()
        message["From"] = f'PMB Politeknik Pos Indonesia<{config.email_alerts}>'
        message["To"] = penerima
        message["Subject"] = "Surat Kelulusan Jalur Rapor POLTEKPOS"
        message["Bcc"] = penerima
        attachment = "assets/template/poltekpos.png"
        html = open("assets/template/email/email_poltekpos.html")
        message.attach(MIMEText(html.read().replace("{attachment}", attachment).replace("{USERNAME}", username).replace("{PASSWORD}", password).replace("{NAMA}", nama).replace("{NOMOR_VA}", nomor_va), "html"))
        #message.attach(MIMEText(body, "plain"))
        fp = open(attachment, 'rb')                                                    
        img = MIMEImage(fp.read())
        fp.close()
        img.add_header('Content-ID', '<{}>'.format(attachment))
        message.attach(img)
        with open(f'{file}', "rb") as fileattachment:
            part = MIMEBase("application", "octet-stream")
            part.set_payload(fileattachment.read())
        encoders.encode_base64(part)
        part.add_header(
            "Content-Disposition",
            "attachment; filename= %s " % file.split("/")[-1:][0],
        )
        message.attach(part)
        text = message.as_string()
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
            server.login(sender_email, sender_pass)
            server.sendmail(sender_email, penerima, text)
        print(f'Email Poltekpos JPA berhasil dikirim ke {penerima}')
        return True
    except Exception as e: 
        print(str(e))
        return False

def kirimEmailFile(penerima, subject, body, file):
    try:
        sender_email = config.email_alerts
        sender_pass = config.email_alerts_password
        message = MIMEMultipart()
        message["From"] = f'PMB <{config.email_alerts}>'
        message["To"] = penerima
        message["Subject"] = subject
        message["Bcc"] = penerima
        message.attach(MIMEText(body, "plain"))
        with open(f'{file}', "rb") as attachment:
            part = MIMEBase("application", "octet-stream")
            part.set_payload(attachment.read())
        encoders.encode_base64(part)
        part.add_header(
            "Content-Disposition",
            "attachment; filename= %s " % file,
        )
        message.attach(part)
        text = message.as_string()
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
            server.login(sender_email, sender_pass)
            server.sendmail(sender_email, penerima, text)

        print(f'Email File {file} berhasil dikirim ke {penerima}')
        return True
    except FileNotFoundError:
        print("File tidak ditemukan")
        return False
    except Exception as e: 
        print(str(e))
        return False