import flask
from flask import request, jsonify, render_template, flash, redirect, url_for, redirect, session, send_file, Markup
import datetime
import uuid
from werkzeug.security import check_password_hash
from apscheduler.schedulers.background import BackgroundScheduler
scheduler = BackgroundScheduler()
scheduler.start()
import os
import markdown

# Config
from helpers.database import *
from helpers.message import *

# Module
import module.pmb as pmb
import module.workerpdf as pdf


app = flask.Flask(__name__)
app.config["DEBUG"] = False
app.config['SECRET_KEY'] = "rahasia"

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == "POST":
        loginData = getLoginData(request.form["username"])
        password = request.form["password"]
        if loginData and check_password_hash(loginData["password"], password):
            session["user"] = loginData["nama"]
            return redirect(url_for("web_home"))
        else:
            flash("Email atau User Salah!")
            return render_template('login.html')
    else:
      return render_template('login.html')

@app.route('/logout')
def logout():
    session.pop("user", None)
    return redirect(url_for("login"))

#HOME
@app.route('/', methods=['GET', 'POST'])
def web_home():
  if "user" in session:
    user = session["user"]
    if request.method == 'POST':
      if request.form["pilihan"] == "undangan":
        f = request.files['file']
        uid = uuid.uuid4()
        f.save(f"assets/userfile/{uid}{f.filename}")
        hasil = pmb.masukinDataPMB(uid,f.filename)
        if not hasil[1]:
          flash(f"{hasil[0]} telah dimasukkan")
        else:
          flash(f"{hasil[0]} telah dimasukkan")
          flash(Markup(f'Tolong cek data pada file ini: <a href="/downloads/{hasil[1]}">Klik disini untuk mendownload file</a>'))
      elif request.form["pilihan"] == "jpa":
        f = request.files['file']
        uid = uuid.uuid4()
        f.save(f"assets/userfile/{uid}{f.filename}")
        hasil = pmb.masukinDataPMBJPA(uid,f.filename)
        if not hasil[1]:
          flash(f"{hasil[0]} telah dimasukkan")
        else:
          flash(f"{hasil[0]} telah dimasukkan")
          flash(Markup(f'Tolong cek data pada file ini: <a href="/downloads/{hasil[1]}">Klik disini untuk mendownload file</a>'))
      elif request.form["pilihan"] == "undangansertifikat":
        f = request.files['file']
        uid = uuid.uuid4()
        f.save(f"assets/userfile/{uid}{f.filename}")
        hasil = pmb.masukinDataPMBSertifikat(f"{uid}{f.filename}")
        if not hasil[1]:
          flash(f"{hasil[0]} telah dimasukkan")
        else:
          flash(f"{hasil[0]} telah dimasukkan")
          flash(Markup(f'Tolong cek data pada file ini: <a href="/downloads/{hasil[1]}">Klik disini untuk mendownload file</a>'))
      scheduler.add_job(pmb.executeTambahContact, id="TambahContact")
      return redirect(url_for('web_home'))
    else:
      return render_template('index.html', 
                                          data        = pmb.dataPesan(), 
                                          datajpa     = pmb.dataPesanJPA(), 
                                          dataevent   = pmb.dataPesanEvent(),
                                          user        = user, 
                                          bot_status  = waCekStatus(), 
                                          sisa_job    = [len(pmb.listJob), len(pmb.listGoogleContact)])
  else:
      return redirect(url_for("login"))

@app.route('/downloads/<path:filename>', methods=['GET', 'POST'])
def download(filename):
    return send_file(f'assets/temp/{filename}.xlsx', as_attachment=True)

@app.route('/tambah_keyword', methods=['GET', 'POST'])
def web_tambah_keyword():
  if "user" in session:
    user = session["user"]
    if request.method == 'POST':
      keyword = request.form["keyword"]
      respond = request.form["respond"]
      if insertKeywordChatbot(keyword, respond):
        flash(f'Keyword "{keyword}" sudah berhasil ditambah')
      else:
        flash("Keyword gagal ditambahkan")
      return render_template('tambah_keyword.html', user = user, data = getDataKeywordAll())
    return render_template('tambah_keyword.html', user = user, data = getDataKeywordAll())
  else:
    return redirect(url_for("login"))

@app.route('/edit_keyword', methods=['GET', 'POST'])
def web_edit_keyword():
  if "user" in session:
    user = session["user"]
    if request.method == 'POST':
      id = request.form["id"]
      keyword = request.form["keyword"]
      respond = request.form["respond"]
      if updateKeywordChatbot(keyword,respond,id):
        flash(f'Keyword "{keyword}" sudah berhasil Diedit')
      else:
        flash("Keyword gagal diubah")
      return redirect(url_for("web_tambah_keyword"))
    if "id" in request.args:
      return render_template('edit/edit_keyword.html', user = user, data = cariDataKeyword(request.args["id"]))
    else:
      return redirect(url_for("web_tambah_template"))
  else:
    return redirect(url_for("login"))

@app.route('/tambah_template', methods=['GET', 'POST'])
def web_tambah_template():
  if "user" in session:
    user = session["user"]
    if request.method == 'POST':
      key = request.form["key"]
      value = request.form["value"]
      if insertTemplateBlast(key,value):
        flash(f'Template "{value}" sudah berhasil ditambah')
      else:
        flash("Template gagal ditambahkan")
      return render_template('tambah_template.html', user = user, data = getDataTemplatePesanAll())
    return render_template('tambah_template.html', user = user, data = getDataTemplatePesanAll())
  else:
    return redirect(url_for("login"))

@app.route('/tambah_event', methods=['GET', 'POST'])
def web_tambah_event():
  if "user" in session:
    user = session["user"]
    if request.method == 'POST':
      nama_event = request.form["nama_event"]
      body_email = request.form["bodyEmail"]
      kampus = request.form["kampus"]
      if kampus == "poltekpos":
        if insertDataEventBLAST(f"[POLTEKPOS] {nama_event}", None, None, body_email):
          flash("Template Blast sudah ditambahkan")
        else:
          flash("Gagal Menambahkan Template")
      elif kampus == "stimlog":
        if insertDataEventBLAST(f"[STIMLOG] {nama_event}", None, None, body_email):
          flash("Template Blast sudah ditambahkan")
        else:
          flash("Gagal Menambahkan Template")
      else:
        flash("Kampus tidak ada")
      return render_template('tambah/event.html', user = user, date=datetime.datetime.now().strftime("%Y-%m-%d"), events = lihatDataEvent())
    return render_template('tambah/event.html', user = user, date=datetime.datetime.now().strftime("%Y-%m-%d"), events = lihatDataEvent())
  else:
    return redirect(url_for("login"))

@app.route('/api/v1/tambah_event_pdf', methods=['GET', 'POST'])
def api_tambah_event_pdf():
  if "user" in session:
    user = session["user"]
    if request.method == 'POST':
      nama_event = request.form["nama_event"]
      body_email = request.form["bodyEmailPDF"]
      kampus = request.form["kampus"]
      pdf = request.files["filepath"]
      filename_pdf = f'{request.form["nama_file"]}.{pdf.filename.split(".")[1]}'
      pdf.save(f"assets/template/sertifikat/{filename_pdf}")
      if kampus == "poltekpos":
        if insertDataEventBLAST(f"[POLTEKPOS] {nama_event}", filename_pdf, request.form["nama_file"], body_email):
          flash("Template Blast sudah ditambahkan")
        else:
          flash("Gagal Menambahkan Template")
      elif kampus == "stimlog":
        if insertDataEventBLAST(f"[STIMLOG] {nama_event}", filename_pdf, request.form["nama_file"], body_email):
          flash("Template Blast sudah ditambahkan")
        else:
          flash("Gagal Menambahkan Template")
      else:
        flash("Kampus tidak ada")
      return render_template('tambah/event.html', user = user, date=datetime.datetime.now().strftime("%Y-%m-%d"), events = lihatDataEvent())
    return render_template('tambah/event.html', user = user, date=datetime.datetime.now().strftime("%Y-%m-%d"), events = lihatDataEvent())
  else:
    return redirect(url_for("login"))

@app.route('/generate_template', methods=['GET'])
def web_generate_template():
  if "user" in session:
    user = session["user"]
    if "id" in request.args:
      data = cariDataEvent(request.args["id"])
      #pmb.generateSertifikat("Test Generate Sertifikat", data["path_file"])
      pdf.generateSuratEventUndanganKhusus("Nama TEST", data["attachment"], data["path_file"])
      return send_file(f"assets/temp/{data['attachment']} Nama TEST.pdf", as_attachment=True)
    else:
      return redirect(url_for("web_tambah_event"))
  else:
    return redirect(url_for("login"))

@app.route('/delete_template', methods=['GET'])
def web_delete_template():
  if "user" in session:
    user = session["user"]
    if "id" in request.args:
      data = cariDataEvent(request.args["id"])
      os.remove(f'assets/template/sertifikat/{data["path_file"]}')
      if deleteDataEvent(request.args["id"]):
        flash("Template Telah Dihapus")
      else:
        flash("Template Gagal Dihapus")
      return redirect(url_for("web_tambah_event"))
    else:
      return redirect(url_for("web_tambah_event"))
  else:
    return redirect(url_for("login"))
@app.route('/edit_template', methods=['GET', 'POST'])
def web_edit_template():
  if "user" in session:
    user = session["user"]
    if request.method == 'POST':
      id = request.form["id"]
      key = request.form["key"]
      value = request.form["value"]
      if updateTemplateBlast(key,value,id):
        flash(f'Template "{value}" sudah berhasil Diedit')
      else:
        flash("Template gagal diubah")
      return redirect(url_for("web_tambah_template"))
    if "id" in request.args:
      return render_template('edit/edit_template.html', user = user, data = cariDataTemplate(request.args["id"]))
    else:
      return redirect(url_for("web_tambah_template"))
  else:
    return redirect(url_for("login"))


@app.route('/kirimfollowup', methods=['GET', 'POST'])
def web_kirim_followup():
  if "user" in session:
    user = session["user"]
    if request.method == "POST":
      for data in cariDataPMBFollowup(request.form.get('tahun'),request.form.get('jenis'),request.form.get('jalur')):
        waKirimPesan(data["nomor_telepon"], request.form.get('pesan'))
      flash("Pesan Akan dikirimkan")
      return render_template('kirim_followup.html', user = user)
    return render_template('kirim_followup.html', user = user)
  else:
    return redirect(url_for("login"))

@app.route('/datapenerima', methods=['GET', 'POST'])
def web_datapenerima():
  if "user" in session:
    user = session["user"]
    return render_template('data/datapenerima.html', user = user)
  else:
    return redirect(url_for("login"))

@app.route('/datapenerimajpa', methods=['GET', 'POST'])
def web_datapenerimajpa():
  if "user" in session:
    user = session["user"]
    return render_template('data/datapenerimajpa.html', user = user)
  else:
    return redirect(url_for("login"))

@app.route('/datapenerimaundangansertifikat', methods=['GET', 'POST'])
def web_datapenerimaundangansertifikat():
  if "user" in session:
    user = session["user"]
    return render_template('data/datapenerimaundangansertifikat.html', user = user, events = lihatDataEvent())
  else:
    return redirect(url_for("login"))

@app.route('/datalogs', methods=['GET'])
def web_datalogs():
  if "user" in session:
    user = session["user"]
    return render_template('datalogs.html', user = user)
  else:
    return redirect(url_for("login"))

@app.route('/api/v1/datapenerima', methods=['GET'])
def api_datapenerima():
  data_semua = []
  for x in lihatDataPMB():
      if x["email"] == None:
        x["email"] = "Tidak ada Email"
      data_semua.append([x["id"], x["nama"], x["asal_sekolah"], x["nomor_telepon"], x["email"], x["tahun"], x["jenis"], x["jalur"]])
  return jsonify({"data": data_semua})

@app.route('/api/v1/datapenerimajpa', methods=['GET'])
def api_datapenerimajpa():
  data_semua = []
  for x in lihatDataPMBJPA():
      data_semua.append([x['id'], x['nama_lengkap'], x['nisn'], x['password'], x['jenis_kelamin'], x['no_telepon'], x['email'], x['perguruan_tinggi'], x['jalur'], x['no_transaksi'], x['virtual_account'], x['jumlah_tagihan'], x['tgl_expired_va'], x['status_pembayaran'], x['tanggal_daftar']])
  return jsonify({"data": data_semua})

@app.route('/api/v1/datapenerimaundangansertifikat', methods=['GET'])
def api_datapenerimaundangansertifikat():
  data_semua = []
  for x in lihatDataPMBUndanganSertifikat():
      if x["email"] == None:
        x["email"] = "Tidak ada Email"
      data_semua.append([x["id"], x["nama"], x["asal_sekolah"], x["nomor_telepon"], x["email"], x["tahun"], x["jenis"], x["jalur"]])
  return jsonify({"data": data_semua})

@app.route('/api/v1/datalogs', methods=['GET'])
def api_datalogs():
  data_semua = []
  for x in lihatLogs():
      data_semua.append([x["nomor_telepon"], x["pesan"], x["timestamp"]])
  return jsonify({"data": data_semua})

@app.route('/api/v1/kirimsemua', methods=['GET', 'POST'])
def api_kirimsemua():
  if request.method == 'POST':
    if cariDataPMBTahun(request.form.get('tahun'),request.form.get('jenis'),request.form.get('jalur')):
      if len(pmb.listJob) == 0:
        pmb.jenisJob = "undangan"
        for data in cariDataPMBTahun(request.form.get('tahun'),request.form.get('jenis'),request.form.get('jalur')):
          pmb.listJob.append({"nama" : data["nama"], "asal_sekolah" : data["asal_sekolah"], "nomor_telepon": data["nomor_telepon"], "email" : data["email"]})
        try:
          scheduler.add_job(pmb.executePesan, id="Pengiriman")
          flash(f"Pesan akan dikirimkan yang tahun {request.form.get('tahun')}")
        except Exception as e:
          flash(f"Telah terjadi error {e} harap hubungi admin")
      else:
        flash("Mohon menunggu semua pesan terkirim")
    else:
      flash("Tidak ada pesan yang akan dikirim")
  return redirect(url_for("web_home"))

@app.route('/api/v1/kirimsemuajpa', methods=['GET', 'POST'])
def api_kirimsemuajpa():
  if request.method == 'POST':
    if len(pmb.listJob) == 0:
      pmb.jenisJob = "jpa"
      for data in lihatDataPMBJPA():
        pmb.listJob.append({"nama_lengkap" : data["nama_lengkap"], "nisn" : data["nisn"], "password" : data["password"], "no_telepon" : data["no_telepon"], "email" : data["email"],  "perguruan_tinggi" : data["perguruan_tinggi"], "no_transaksi" : data["no_transaksi"], "virtual_account" : data["virtual_account"], "jumlah_tagihan" : data["jumlah_tagihan"], "tgl_expired_va" : data["tgl_expired_va"], "status_pembayaran" : data["status_pembayaran"]})
      try:
        scheduler.add_job(pmb.executePesan, id="Pengiriman")
        flash("Pesan akan dikirim kepada penerima jalur jpa")
      except Exception as e:
        flash(f"Telah terjadi error {e} harap hubungi admin")
    else:
      flash("Mohon menunggu semua pesan terkirim")
      
  return redirect(url_for("web_home"))

@app.route('/api/v1/kirimsemuaundangansertifikat', methods=['GET', 'POST'])
def api_kirimsemuaundangansertifikat():
  if request.method == 'POST':
    if len(pmb.listJob) == 0:
      if request.form.get("jenis") == "undangansertifikat":
        event = cariDataEvent(request.form.get("event"))
        pmb.jenisJob = "undangansertifikat"
        for data in lihatDataPMBUndanganSertifikat():
          if data["pengiriman"] == 0:
            pmb.listJob.append({
            "id"            : data["id"],
            "nama"          : data["nama"], 
            "asal_sekolah"  : data["asal_sekolah"], 
            "nomor_telepon" : data["nomor_telepon"], 
            "email"         : data["email"], 
            "nama_event"    : event["nama_event"], 
            "tanggal"       : datetime.datetime.strptime(str(event["tanggal"]), "%Y-%m-%d").strftime("%d %B %Y"), 
            "filepath"      : event["path_file"],
            "attachment"    : event["attachment"],
            "path_email"    : event["path_email"]})
          else:
            pass
        if len(pmb.listJob) == 0:
          flash("Tidak ada data yang akan dikirimkan")
        else:
          try:
            scheduler.add_job(pmb.executePesan, id="Pengiriman")
            flash("Pesan undangan dan sertifikat akan dikirimkan kepada penerima event")
          except Exception as e:
            flash(f"Telah terjadi error {e} harap hubungi admin")
      elif request.form.get("jenis") == "khusussertifikatemail":
        event = cariDataEvent(request.form.get("event"))
        pmb.jenisJob = "khusussertifikatemail"
        for data in lihatDataPMBUndanganSertifikat():
          if data["pengiriman"] == 0:
            pmb.listJob.append({
            "id"            : data["id"],
            "nama"          : data["nama"], 
            "asal_sekolah"  : data["asal_sekolah"], 
            "nomor_telepon" : data["nomor_telepon"], 
            "email"         : data["email"], 
            "nama_event"    : event["nama_event"], 
            "tanggal"       : datetime.datetime.strptime(str(event["tanggal"]), "%Y-%m-%d").strftime("%d %B %Y"), 
            "filepath"      : event["path_file"],
            "attachment"    : event["attachment"],
            "path_email"    : event["path_email"]})
          else:
            pass
        if len(pmb.listJob) == 0:
          flash("Tidak ada data yang akan dikirim")
        else:
          try:
            scheduler.add_job(pmb.executePesan, id="Pengiriman")
            flash("Pesan sertifikat akan dikirimkan kepada penerima event")
          except Exception as e:
            flash(f"Telah terjadi error {e} harap hubungi admin")
      elif request.form.get("jenis") == "khususemailstimlog":
        event = cariDataEvent(request.form.get("event"))
        pmb.jenisJob = "khususemailstimlog"
        for data in lihatDataPMBUndanganSertifikat():
          if data["pengiriman"] == 0:
            pmb.listJob.append({
            "id"            : data["id"],
            "nama"          : data["nama"], 
            "asal_sekolah"  : data["asal_sekolah"], 
            "nomor_telepon" : data["nomor_telepon"], 
            "email"         : data["email"], 
            "nama_event"    : event["nama_event"], 
            "tanggal"       : datetime.datetime.strptime(str(event["tanggal"]), "%Y-%m-%d").strftime("%d %B %Y"), 
            "filepath"      : event["path_file"],
            "attachment"    : event["attachment"],
            "isi_email"    : event["isi_email"]})
          else:
            pass
        if len(pmb.listJob) == 0:
          flash("Tidak ada data yang akan dikirim")
        else:
          try:
            scheduler.add_job(pmb.executePesan, id="Pengiriman")
            flash("Pesan sertifikat akan dikirimkan kepada penerima event")
          except Exception as e:
            flash(f"Telah terjadi error {e} harap hubungi admin")
      elif request.form.get("jenis") == "khususemailpoltekpos":
        event = cariDataEvent(request.form.get("event"))
        pmb.jenisJob = "khususemailpoltekpos"
        for data in lihatDataPMBUndanganSertifikat():
          if data["pengiriman"] == 0:
            pmb.listJob.append({
            "id"            : data["id"],
            "nama"          : data["nama"], 
            "asal_sekolah"  : data["asal_sekolah"], 
            "nomor_telepon" : data["nomor_telepon"], 
            "email"         : data["email"], 
            "nama_event"    : event["nama_event"], 
            "tanggal"       : datetime.datetime.strptime(str(event["tanggal"]), "%Y-%m-%d").strftime("%d %B %Y"), 
            "filepath"      : event["path_file"],
            "attachment"    : event["attachment"],
            "isi_email"    : event["isi_email"]})
          else:
            pass
        if len(pmb.listJob) == 0:
          flash("Tidak ada data yang akan dikirim")
        else:
          try:
            scheduler.add_job(pmb.executePesan, id="Pengiriman")
            flash("Pesan sertifikat akan dikirimkan kepada penerima event")
          except Exception as e:
            flash(f"Telah terjadi error {e} harap hubungi admin")
      elif request.form.get("jenis") == "khususemailstimlogPDF":
        event = cariDataEvent(request.form.get("event"))
        pmb.jenisJob = "khususemailstimlogPDF"
        for data in lihatDataPMBUndanganSertifikat():
          if data["pengiriman"] == 0:
            pmb.listJob.append({
            "id"            : data["id"],
            "nama"          : data["nama"], 
            "asal_sekolah"  : data["asal_sekolah"], 
            "nomor_telepon" : data["nomor_telepon"], 
            "email"         : data["email"], 
            "nama_event"    : event["nama_event"], 
            "tanggal"       : datetime.datetime.strptime(str(event["tanggal"]), "%Y-%m-%d").strftime("%d %B %Y"), 
            "filepath"      : event["path_file"],
            "attachment"    : event["attachment"],
            "isi_email"    : event["isi_email"]})
          else:
            pass
        if len(pmb.listJob) == 0:
          flash("Tidak ada data yang akan dikirim")
        else:
          try:
            scheduler.add_job(pmb.executePesan, id="Pengiriman")
            flash("Pesan sertifikat akan dikirimkan kepada penerima event")
          except Exception as e:
            flash(f"Telah terjadi error {e} harap hubungi admin")
      elif request.form.get("jenis") == "khususemailpoltekposPDF":
        event = cariDataEvent(request.form.get("event"))
        pmb.jenisJob = "khususemailpoltekposPDF"
        for data in lihatDataPMBUndanganSertifikat():
          if data["pengiriman"] == 0:
            pmb.listJob.append({
            "id"            : data["id"],
            "nama"          : data["nama"], 
            "asal_sekolah"  : data["asal_sekolah"], 
            "nomor_telepon" : data["nomor_telepon"], 
            "email"         : data["email"], 
            "nama_event"    : event["nama_event"], 
            "tanggal"       : datetime.datetime.strptime(str(event["tanggal"]), "%Y-%m-%d").strftime("%d %B %Y"), 
            "filepath"      : event["path_file"],
            "attachment"    : event["attachment"],
            "isi_email"    : event["isi_email"]})
          else:
            pass
        if len(pmb.listJob) == 0:
          flash("Tidak ada data yang akan dikirim")
        else:
          try:
            scheduler.add_job(pmb.executePesan, id="Pengiriman")
            flash("Pesan sertifikat akan dikirimkan kepada penerima event")
          except Exception as e:
            flash(f"Telah terjadi error {e} harap hubungi admin")
      else:
        print("Lain")
    else:
      flash("Mohon menunggu semua pesan terkirim")
  return redirect(url_for("web_home"))
  
@app.route('/api/v1/resetpengiriman', methods=['GET', 'POST'])
def api_resetpengiriman():
  pmb.listJob = []
  flash("Pengiriman sudah direset")
  return redirect(url_for("web_home"))

@app.route('/api/v1/mintasurat', methods=['POST'])
def api_mintasurat():
  if request.method == "POST":
    nama = request.json["nama"]
    asal_sekolah = request.json["asal_sekolah"]
    if pmb.generateSuratUndangan(nama, asal_sekolah):
        path = f"assets/template/Surat Undangan {nama}.pdf"
        data = pmb.encodeFileData(path)
    return jsonify({"pesan":data[0], "base64":data[1], "mime_type":data[2]}), 200
  else:
    return jsonify("Please Use The Correct Format"),400

if __name__ == "__main__":
  app.run(host='0.0.0.0', debug=True)
